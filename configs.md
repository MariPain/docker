**Etape 1: Création de l'infrastructure Azure**
Utilization de CLI azure pour l'infrastructure:

Utilisez le document Azure_infra pour obtenir les informations sur l'infrastructure à mettre en place.
Créez vos groupes de ressources selon les informations fournies dans le document.

```
Nome:NOME 
prenome: PRENOME
grupo de recursos: OCC_ASD_Prenome
nombre de vnet: Vnet_OCC_ASD_Prenome
plage: /24
nombre de subnet: Subnet_Vnet_OCC_ASD_Prenome
plage: /28
subnet admin:  10.0.16.0/28 -10.0.16.15/28
soit: 16  sous réseaux

```

Tapez la comand :

```cli
az network vnet create --name Vnet_OCC_ASD_Maria-Dolores 
>--resource-group OCC_ASD_Maria-Dolores 
>--address-prefix 10.0.16.0/24 
>--subnet-name Subnet_Vnet_OCC_ASD_Maria-Dolores 
>--subnet-prefixes 10.0.16.0/28
```

definir un script:

```bash
### version 2 de creacion de bucle de subnets 

## Definir los parámetros
vnet_nombre="Vnet_OCC_ASD_Maria-Dolores"
grupo_recursos="OCC_ASD_Maria-Dolores"
subnet_nombre="Subnet_Vnet_OCC_ASD_Maria-Dolores"
subnet_prefix="10.0.16.0/28"

# Crear la primera subred
az network vnet subnet create \
    --name "$subnet_nombre-0" \
    --resource-group "$grupo_recursos" \
    --vnet-name "$vnet_nombre" \
    --address-prefix "$subnet_prefix"

# Bucle para generar la nomenclatura y crear las subredes restantes
for i in {1..15}; do
    nombre_subred="$subnet_nombre-$i"
    direccion_ip="10.0.16.$((i*16))/28"
    az network vnet subnet create \
        --name "$nombre_subred" \
        --resource-group "$grupo_recursos" \
        --vnet-name "$vnet_nombre" \
        --address-prefix "$direccion_ip"
done
```
Que hace el script??

Este script primero crea la subred inicial "Subnet_Vnet_OCC_ASD_Prenome-0" y luego crea las subredes restantes con nombres como "Subnet_Vnet_OCC_ASD_Prenome-1", "Subnet_Vnet_OCC_ASD_Prenome-2", etc. con los rangos de direcciones IP especificados. Ejecuta este script en tu terminal Bash para crear las subredes como se describe.


resultado:

"Subnet_Vnet_OCC_ASD_Maria-Dolores-0: 10.0.16.0/28"
"Subnet_Vnet_OCC_ASD_Maria-Dolores-1: 10.0.16.16/28"
"Subnet_Vnet_OCC_ASD_Maria-Dolores-2: 10.0.16.32/28"
"Subnet_Vnet_OCC_ASD_Maria-Dolores-3: 10.0.16.48/28"
"Subnet_Vnet_OCC_ASD_Maria-Dolores-4: 10.0.16.64/28"
"Subnet_Vnet_OCC_ASD_Maria-Dolores-5: 10.0.16.80/28"
"Subnet_Vnet_OCC_ASD_Maria-Dolores-6: 10.0.16.96/28"
"Subnet_Vnet_OCC_ASD_Maria-Dolores-7: 10.0.16.112/28"
"Subnet_Vnet_OCC_ASD_Maria-Dolores-8: 10.0.16.128/28"
"Subnet_Vnet_OCC_ASD_Maria-Dolores-9: 10.0.16.144/28"
"Subnet_Vnet_OCC_ASD_Maria-Dolores-10: 10.0.16.160/28"
"Subnet_Vnet_OCC_ASD_Maria-Dolores-11: 10.0.16.176/28"
"Subnet_Vnet_OCC_ASD_Maria-Dolores-12: 10.0.16.192/28"
"Subnet_Vnet_OCC_ASD_Maria-Dolores-13: 10.0.16.208/28"
"Subnet_Vnet_OCC_ASD_Maria-Dolores-14: 10.0.16.224/28"
"Subnet_Vnet_OCC_ASD_Maria-Dolores-15: 10.0.16.240/28"



